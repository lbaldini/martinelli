# README #

Questa pagina ospita la versione finale (e ormai fuori catalogo) delle
dispense, originariamente edite da ETS, ed utilizzate nel corso di Laboratorio 1
presso l'Università di Pisa sino all'anno 2015: Liana Martinelli e Luca Baldini,
*"Misure ed analisi dei dati: introduzione al Laboratorio di Fisica "*,
[ISBN: 978-884671616-3](http://www.edizioniets.com/scheda.asp?n=978-884671616-3).

Il materiale è nel pubblico dominio secondo la
[Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/), con il consenso della Prof.ssa Martinelli e della casa
editrice ETS, che ringrazio entrambe.

La versione pdf delle dispense è disponibile sulla pagina dei
[download](https://bitbucket.org/lbaldini/martinelli/downloads/).
