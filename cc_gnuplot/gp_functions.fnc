
# Constant.
c = 1
constant(x) = c

# Straight line.
a = 1
b = 1
line(x) = a*x + b

# Degree 10 polinomials.
c_10= 1
c_9 = 1
c_8 = 1
c_7 = 1
c_6 = 1
c_5 = 1
c_4 = 1
c_3 = 1
c_2 = 1
c_1 = 1
c_0 = 1
pol10(x) = c_10*x**10 + c_9*x**9 + c_8*x**8 + c_7*x**7 + c_6*x**6 + c_5*x**5 +\
	c_4*x**4 + c_3*x**3 + c_2*x**2 + c_1*x + c_0
